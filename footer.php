<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package integration
 */

?>

	</div><!-- #content -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col--50">
					<div class="text-wrap">
						<div class="footer-text protection ">Все права защищены</div>
						<div class="footer-text privacy "><a href="#" target="_blank">Политика конфиденциальности</a></div>
					</div>	
				</div>
				<div class="col--50">
					<div class="footer-text development">Сайт разработан компанией <a href="https://ayeps.ru/" target="_blank">AYEP'S</a></div>
				</div>
			</div>
		</div>
	</footer>
	
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Preloader Script -->
<script>$(window).on('load', function() {$('.preloader').delay(500).fadeOut(500);});</script>
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

</body>
</html>
