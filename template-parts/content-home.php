<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package integration
 */

?>

		<?php //the_content(); ?>

		<section class="s-offer bg-white">
			<div class="title-section">
				<h2>Предлагаем</h2>
			</div>
			<div class="content-offer">
				<div class="container">
					<div class="row">
						<div class="col--50">
							<div class="banner-block">
								<div class="banner-img">
									<img class="icon-banner" src="<?echo get_template_directory_uri() . '/img/s-offer/circle-11.png' ?>" alt="" >
								</div>
								<div class="banner-text">Представительство в суде по делам о наследстве</div>
							</div>
						</div>
						<div class="col--50">
							<div class="banner-block">
								<div class="banner-img">
									<img class="icon-banner" src="<?echo get_template_directory_uri() . '/img/s-offer/circle-22.png' ?>" alt="" >
								</div>
								<div class="banner-text">Оформление у натариуса наследственных прав и иных прав, следующих из открытия наследства</div>
								 <div style="clear: both"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="s-why">
			<div class="title-section">
				<h2>Почему мы</h2>
			</div>
			<div class="section-content bg-grey">
				<div class="container">
					<div class="row">
						<div class="col--30">
							<img class="parker" src="<?echo get_template_directory_uri() . '/img/s-why/parker.jpg' ?>" alt="" >
							<div style="clear: both"></div>
						</div>
						<div class="col--70">
							<div class="info">
								<img src="<?echo get_template_directory_uri() . '/img/s-why/icon-11.png' ?>" class="info-icon" alt="">
								<p>Мы <span>специализируемся</span> на наследственном праве.</p>
							</div>
							<div class="info">
								<img src="<?echo get_template_directory_uri() . '/img/s-why/icon-22.png' ?>" class="info-icon" alt="">
								<p>В оформлении наследственных прав участвуют специалисты со стажем работы в этой области <span>более 15 лет</span>.</p>
							</div>
							<div class="info">
								<img src="<?echo get_template_directory_uri() . '/img/s-why/icon-33.png' ?>" class="info-icon" alt="">
								<p>Для выработки правовой позиции привлекаются <span>лучшие из лучших</span> специалисты наследственного права.</p>
							</div>
							<div class="info">
								<img src="<?echo get_template_directory_uri() . '/img/s-why/icon-44.png' ?>" class="info-icon" alt="">
								<p>При определении тактики достижения цели, используется <span>принцип соразмерности</span>.</p>
							</div>
							<div style="clear: both"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="s-contacts bg-white" id="contacts">
			<div class="title-section">
				<h2>Контакты</h2>
			</div>
			<div class="section-content bg-black bg--cover" style="background-image: url(<?echo get_template_directory_uri() . '/img/s-contacts/books.jpg' ?>)">
				<div class="container">
					<div class="row">
						<div class="col--50 left">
							<div class="contact-wrapper wrapper-left">
								<div class="connect">
									<p><i class="fa fa-envelope-o" aria-hidden="true"></i>Почта</p>
									<p><a href="mailto:1234567@gmail.com">1234567@gmail.com;</a></p>
									 <div style="clear: both"></div>
								</div>
								<div class="connect">
									<p><i class="fa fa-phone" aria-hidden="true"></i>Телефон</p>
									<p><a href="tel:+70000000000">+700 000 000 00</a></p>
									<div style="clear: both"></div>
								</div>
							</div>
						</div>
						<div class="col--50 right">
							<div class="contact-wrapper wrapper-right">
								<div class="connect">
									<p><i class="fa fa-commenting-o" aria-hidden="true"></i>Чат</p>
								</div>
								<form class="form-chat" method="post" action="#">
									<div class="form-body">
										<textarea name="message" class="textarea" placeholder="Введите сообщение" ></textarea>
									</div>
									<div class="button-wrap">
										<button type="submit" class="btn">Отправить</button> 
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
