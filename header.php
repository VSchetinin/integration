<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package integration
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.5, user-scalable=yes"/>
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Partners</title>
<meta name="description" content="">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.5, user-scalable=yes"/>
<link rel="shortcut icon" href="#" type="image/x-icon">
<style>body{opacity:1; overflow-x:hidden;}.preloader{position:fixed;top:0;left:0;bottom:0;right:0;z-index:10000;background-color:#3F70A9;}</style>
<?php wp_head(); ?>
</head>

<body <?php //body_class(); ?> class="home">
<div class="preloader"></div>

<div id="page" class="site">
	<!--a class="skip-link screen-reader-text" href="#content"><?php// esc_html_e( 'Skip to content', 'integration' ); ?></a-->

	<div class="head-wrapper">
		<section class="head-section">
			<div class="top-line">
				<h2 class="top-title row-center"><a href="#contacts" class="ancLinks">Контакты</a></h2>
			</div>
			<div class="head-title">
				<div class="logo-wrap row-center">
					<img src="<?echo get_template_directory_uri() . '/img/head/logotype.png' ?>" class="logotype" alt="">
				</div>
				<h1 class="logo row-center">Люлюшина и партнеры</h1>
			</div>
			<div class="scrollDown">
				<p><span></span></p>
			</div>
		</section>
	</div>	

	<div id="content" class="site-content">
